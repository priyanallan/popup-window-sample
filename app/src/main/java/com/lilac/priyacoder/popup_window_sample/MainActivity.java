package com.lilac.priyacoder.popup_window_sample;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button popupButton = findViewById(R.id.popupbutton);

        popupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.relative_layout);
                LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.popup,null);
                layout.setPadding(40,40,40,40);
                PopupWindow pw = new PopupWindow(layout,LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT,true);
//                pw.showAtLocation(layout, Gravity.CENTER,0,0);
                pw.showAsDropDown(popupButton,0,0);
                WindowManager.LayoutParams windowManager = getWindow().getAttributes();
                windowManager.dimAmount = 0.75f;
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

            }
        });
    }

}
